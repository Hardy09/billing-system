import React from 'react';
import db from '../Database/firestore.js'
import * as page from '../enum/enum.js';
import Show_Expenses_info from '../Expenses/Show_expenses_info.js';

class Show_Users_info extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            pending : this.props.show_user_info.Pending_Amount,
            final : this.props.show_user_info.Final_Amount,
            Email : "",
            list_of_order_of_customer : [],
            Order_Of_Cust : "",
            fetched_data_expenses : [],
            flag : page.Pages.Show_table,
        };
    }

    componentDidMount(){
        this.findName();
        console.log('GrandChild did mount.');
    }

     Pending_Amount_Changed = (event) => {
        console.log("Pending amount changed");
        this.setState({
            ...this.state,
            pending : event.target.value,
        });
    }

    Final_Amount_Changed = (event) => {
        console.log("Final amount changed");
        this.setState({
            ...this.state,
            final : event.target.value,
        });
    }

     Update_Data_on_Firestore = () => {
         console.log("======================",this.props.show_user_info);
        db.firestore().collection('Users').doc(this.props.show_user_info.id).set({
            "Name" : this.props.show_user_info.Name,
            "Email" : this.props.show_user_info.Email,
            "Pending_Amount" : this.state.pending,
            "Final_Amount" : this.state.final,
        }).then(function(){
            console.log("Data Successfully Updated");
        }).catch(function(err){
            console.log(err);
        });
    }

       findName = () => {
            let pay_type = this.props.show_user_info.Name;
            var response = [];
            db.firestore().collection("Expense_Data")
                .where('Payment_Type', '==', pay_type).get()
                .then(it => {
                    it.forEach((val) =>  {
                        console.log("DATA",val.data());
                        response.push({...val.data(),id: val.id});
                    });
                    console.log("DATA IN RESPONSE",response);
                    let len = response.length;
                    this.setState({
                        ...this.state,
                        list_of_order_of_customer : response,
                        Order_Of_Cust : len,
                    });
            });
        };

    fetch_list_data = () => {
        var l = this.state.list_of_order_of_customer;
        var response = [];
        l.map((value,index) => {
            console.log("FETCH LIST DATAAAAAAAAAA",value.Expenses[0].name);
              var b = (
                   <tr key={value.id} id="customer_tr" onClick={this.fetch_Particular_Expense_Details.bind(this,index)}>
                       <td id="customer_td">{value.Payment_Type}</td>
                       <td id="customer_td">{value.Expenses[0].name}</td>
                       <td id="customer_td">{value.Expenses[0].type}</td>
                       <td id="customer_td">{value.Total_expense_cost}</td>
                       <td id="customer_td">{value.Date}</td>
                   </tr>
              );
              response.push(b);
        });
        return response;
    }

    fetch_Particular_Expense_Details = (index) => {
        var data_list  = this.state.list_of_order_of_customer[index];
            console.log("The Value IS:", data_list);
            this.setState({
                 ...this.state,
                 flag : page.Pages.Show_table_values_Expenses,
                 fetched_data_expenses : data_list,
            });
    }

    showtotal = () => {
        var l = this.state.list_of_order_of_customer;
        let sum = 0;
        l.forEach(value => {
              sum += parseInt(value.Total_expense_cost);
        });
        return sum;
    }

    table_part = () => {
        return (
            <div>
                    <div>Total Balance for {this.props.show_user_info.Name} :  {this.showtotal()}</div>
                    <div> Total Number of expenses for {this.props.show_user_info.Name} are : {this.state.Order_Of_Cust} </div>
                      <div id="add_margin"></div>
                        <table id="customer_table">
                            <thead>
                                <tr id="customer_tr">
                                    <th id="customer_th">Expense_Payment_type</th>
                                    <th id="customer_th">Expense_Name</th>
                                    <th id="customer_th">Expense_Type</th>
                                    <th id="customer_th">Expense_Amount</th>
                                    <th id="customer_th">Exp_Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.fetch_list_data()}
                            </tbody>
                        </table>
                </div>
        );
    }

    check = () => {
        var flag = this.state.flag;
        if(flag === page.Pages.Show_table){
            return this.table_part();
        }else{
            return <Show_Expenses_info show_Expenses_info ={this.state.fetched_data_expenses}/>;
        }
    }



         render(){
            console.log("IN SHOW USERS INFO PAGE", this.props.show_user_info)
            return (
                <div>
                    {this.check()}
                </div>
            );
         }
}

export default Show_Users_info;
