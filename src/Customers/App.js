import React from 'react';
import logo from '../logo.svg';
import '../css/App.css';
import 'bootstrap/dist/css/bootstrap.css';
import db from '../Database/firestore.js'
import io from 'socket.io-client';
import * as page from '../enum/enum.js';
import Show_Users_Info from './Show_Users_Info.js';
//import Expenses from '../Expenses.js';
import Expenses_Part_2 from '../Expenses/Expenses_Part_2.js';
import Sign_Up from '../SignUp/Sign_Up.js';
//import Build_logic from '../Building_Expense_logic.js';


class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Customers_Expenses_flag : true,
            res : [],
            flag : page.Pages.Show_table,
            name : "",
            Email : "",
            pending_amt : "",
            final_amt : "",
            list : [],
            fetched_data : {},
            user_log : false,
        }
    }

    componentDidMount(){
        this.Get_data_from_firestore();
        this.User_logged_In();
        console.log('GrandChild did mount.');
    }

    NameChange = (event) => {
        this.setState({
            ...this.state,
            name : event.target.value,
        });
    }

    EmailChange = (event) => {
        this.setState({
            ...this.state,
            Email : event.target.value,
        });
    }

    Pending_Amount_Changed = (event) => {
        this.setState({
            ...this.state,
            pending_amt : event.target.value,
        });
    }

    Final_Amount_Changed = (event) => {
        this.setState({
            ...this.state,
            final_amt : event.target.value,
        });
    }

    Add_Data_to_Firestore = (event) => {
        event.preventDefault();
        db.firestore().collection('Users').add({
            "Name" : this.state.name,
            "Email" : this.state.Email,
            "Pending_Amount" : this.state.pending_amt,
            "Final_Amount" : this.state.final_amt
        }).then(function(){
            console.log("Data Successfully Entered");
        }).catch(function(err){
            console.log(err);
        });
        this.setState({
              ...this.state,
              flag : page.Pages.Show_table,
        });
    }

    Get_data_from_firestore = () => {
         db.firestore().collection("Users").onSnapshot(it => {
//              it.docChanges().forEach(change => {
//                if (change.type === 'modified') {
//                        console.log('Modified Data: ', change.doc.data());
//                }
//              });
              var response = [];
              it.forEach((doc) => {
                response.push({...doc.data(),id: doc.id})
              })
              this.setState({
                    ...this.state,
                    list : response,
              })
            console.log(this.state.list);
        });
    }



    fetch_Particular_User_Details = (index) => {
            var data_list  = this.state.list[index];
            console.log("The Value IS:", data_list);
            this.setState({
                 ...this.state,
                 flag : page.Pages.Show_table_values,
                 fetched_data : data_list,
            });
     }

    fetch_list_data = () => {
        var l = this.state.list;
        var response = [];
//        l.forEach((li) => {
//            console.log("IN FOR EACH");
//            console.log(li);
//        });
        l.map((value,index) => {
            console.log(value.Name);
              var b = (
                   <tr key={value.id} onClick={this.fetch_Particular_User_Details.bind(this,index)} id="customer_tr">
                       <td id="customer_td">{value.Name}</td>
                       <td id="customer_td">{value.Email}</td>
                       <td id="customer_td">{value.Final_Amount}</td>
                       <td id="customer_td">{value.Pending_Amount}</td>
                   </tr>
              );
              response.push(b);
        });
        return response;
    }

     buttonClick = () => {
        //console.log("Hello");
        this.setState({
            ...this.state,
            flag : page.Pages.InputUser,
        });
    }

     table_func = () => {
        return (
              <div id="customer_div">
                <button onClick={this.buttonClick} id="customer_add_btn">Add Customers</button>
                <div id="dummy">dummy</div>
                    <table id="customer_table">
                       <thead>
                            <tr id="customer_tr">
                                <th id="customer_th">Firstname</th>
                                <th id="customer_th">Lastname</th>
                                <th id="customer_th">Pending Amount</th>
                                <th id="customer_th">Final Amount</th>
                            </tr>
                       </thead>
                       <tbody>
                            {this.fetch_list_data()}
                       </tbody>
                  </table>
              </div>
               );
    }


    InputUser = () => {
        return (
            <form className="form-horizontal" onSubmit={this.Add_Data_to_Firestore}>
                <div className="form-group">
                    <label class="control-label col-sm-2" for="name">Name:</label>
                    <div className="col-sm-10">
                        <input type="Name" className="form-control" placeholder="Enter Name" value= {this.state.name}
                        onChange= {this.NameChange} />
                    </div>
                </div>
                 <div className="form-group">
                    <label className="control-label col-sm-2" for="Email">Email:</label>
                    <div class="col-sm-10">
                        <input type="Email" className="form-control" placeholder="Enter Email"
                        value= {this.state.Email} onChange= {this.EmailChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label className="control-label col-sm-2" >Pending Amount:</label>
                    <div class="col-sm-10">
                        <input type="Number" className="form-control" placeholder="Enter Pending Amount"
                        value= {this.state.pending_amt} onChange= {this.Pending_Amount_Changed} />
                    </div>
                </div>
                <div className="form-group">
                    <label className="control-label col-sm-2" for="Email">Final Amount:</label>
                    <div class="col-sm-10">
                        <input type="Number" className="form-control" placeholder="Enter Email"
                        value= {this.state.final_amt} onChange= {this.Final_Amount_Changed} />
                    </div>
                </div>
                <button className="btn btn-primary">Submit</button>
            </form>
        );
    }

    Check = () => {
        var check = this.state.flag;
         if(check === page.Pages.Show_table){
           return this.table_func();
         }else if(check === page.Pages.InputUser){
           return this.InputUser();
         }else if(check === page.Pages.Show_table_values){
           return <Show_Users_Info show_user_info ={this.state.fetched_data}/>;
         }else if(check === page.Pages.Expenses){
            return <Expenses_Part_2 />;
         }else if(check === page.Pages.Customers){
            return this.table_func();
         }else{
            return this.table_func();
         }
    }

//    customer_Expenses_Check = () => {
//        var check = this.state.customer_Expenses_Check;
//        if(check){
//            return this.table_func();
//        }else{
//            return Expenses();
//        }
//    }

    Move_to_Customers = () => {
        this.setState({
            ...this.state,
            flag : page.Pages.Customers,
        });
    }

    Move_to_Expenses = () => {
        this.setState({
            ...this.state,
            flag : page.Pages.Expenses,
        });
    }

    Move_to_Testing = () => {
        this.setState({
            ...this.state,
            flag : page.Pages.Testing,
        });
    }

    User_logged_In = () => {
        var mythis = this;
        console.log("Hello.....");
        db.auth().onAuthStateChanged(function(user) {
             if (user) {
                var uid = user.uid;
                console.log("User___ID", uid);
                mythis.setState({
                    ...mythis.state,
                    user_log : true,
                })

//    // ...
            } else {
    // User is signed out.
                console.log("ERRRRRRRORRRRRRR");
                mythis.setState({
                    ...mythis.state,
                    user_log : false,
                });
            }
        });
//        return <h1>hello</h1>;
    }
//
    Check_Logged_In = () => {
        var check = this.state.user_log;
        if(check){
            console.log("Check",check);
            return this.Dashboard_of_Application();
        }else{
            console.log("Check",check);
            return <Sign_Up/>;
        }
    }

    Dashboard_of_Application = () => {
        return (
        <div class="row">
           <div className="column left">
                    <div id="cus" onClick={this.Move_to_Customers}><label>Customers</label></div>
                    <div id="Exp" onClick={this.Move_to_Expenses}><label>Expenses</label></div>
                    <div id="Test" onClick={this.Move_to_Testing}><label>Testing</label></div>
                </div>
                <div className="column right">
                    <div>
                        {this.Check()}
                    </div>
                </div>
        </div>
        );
    }



    render(){
        return(
            <div>
                {this.Check_Logged_In()}
            </div>
        );
    }
}


export default App;
