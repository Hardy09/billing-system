import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCN7km5mTFTU1XSGy0ComKhDQQXrxlLrQI",
    authDomain: "react-project-9da09.firebaseapp.com",
    databaseURL: "https://react-project-9da09.firebaseio.com",
    projectId: "react-project-9da09",
    storageBucket: "react-project-9da09.appspot.com",
    messagingSenderId: "471061497919",
    appId: "1:471061497919:web:5ebbfde87b626430e5508a",
    measurementId: "G-TFFLT4BGXF"
};

firebase.initializeApp(config);

const db = firebase;

export default db;