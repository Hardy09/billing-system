import React from 'react';
import db from '../Database/firestore.js';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import { Modal } from 'react-bootstrap'

class Show_Expenses_info extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            pending : this.props.show_Expenses_info.Pending_Amount,
            final : this.props.show_Expenses_info.Final_Amount,
        };
    }
//
//     Pending_Amount_Changed = (event) => {
//        console.log("Pending amount changed");
//        this.setState({
//            ...this.state,
//            pending : event.target.value,
//        });
//    }
//
//    Final_Amount_Changed = (event) => {
//        console.log("Final amount changed");
//        this.setState({
//            ...this.state,
//            final : event.target.value,
//        });
//    }
//
//     Update_Data_on_Firestore = () => {
//        console.log("======================",this.props.show_Expenses_info);
//        db.collection('Expenses').doc(this.props.show_Expenses_info.id).set({
//            "Name" : this.props.show_Expenses_info.Name,
//            "Type" : this.props.show_Expenses_info.Type,
//            "Pending_Amount" : this.state.pending,
//            "Final_Amount" : this.state.final,
//            "Qty" : this.props.show_Expenses_info.Qty,
//            "Discount" : this.props.show_Expenses_info.Discount,
//        }).then(function(){
//            console.log("Data Successfully Updated");
//        }).catch(function(err){
//            console.log(err);
//        });
//    }

     calculate_total_discount = () => {
        let lst = this.props.show_Expenses_info.Data;

        let s = 0;
            for(let i=0;i<lst.length;i++){
                s += parseInt(lst[i].discount);

            }
        console.log("Total Discount====",s);
        return s;
     }

     fetch_list_data = () => {
            let lst = this.props.show_Expenses_info;
            let my = this.state.url;
            console.log("list====",lst);
            return lst.Expenses.map((value,index) => {
                 return (
                 <tr key={value.index}>
                    <td><input value = {value.name} name= "name" id="expense_td" readonly size="100"/></td>
                    <td><input value = {value.type} name = "type" readonly size="95"/></td>
                    <td><input value = {value.amount} name = "amount" readonly/></td>
                    <td><a id={index} download>
                    <img src="download.png" width="30" height="30" onClick={this.download_bill.bind(this,index,value.img)} id={`my${index}`}/>
                    </a>
                    </td>
                 </tr>
                );
            })
    }

    download_bill = (i,url) => {
        var URL = "gs://react-project-9da09.appspot.com/"+url;
//        console.log(URL);
        console.log("DOWNLOAD BILLL");
        db.storage().ref().child(url).getDownloadURL().then(function(url) {
            console.log(url);
            var an = document.getElementById(i);
            var img = document.getElementById(`my${i}`);
            img.src = url;
            an.href = url;
            return url;
//            return (
//              <div>
//                <Modal.Dialog>
//                <Modal.Body>
//                    <img src={url} width="100" height="100"/>
//                </Modal.Body>
//                </Modal.Dialog>
//              </div>
//            );
//            var xhr = new XMLHttpRequest();
//            xhr.responseType = 'blob';
//            xhr.onload = function(event) {
//                var blob = xhr.response;
//            };
//            xhr.open('GET', URL);
//            xhr.send();
////         Or inserted into an <img> element:
////        var img = document.getElementById('myimg');
////        img.src = url;
        }).catch(function(error) {
            console.log(error);
        });
    }

           render (){
                return (
            <div id="expense_div">

                <div className="row" id="my">
                    <div className="column_of_expense_left">
                        <div id="row_of_expense">INVOICE</div>
                        <div id="row_of_expense">SURELOCAL SUPPLY CHAIN pvt.ltd</div>
                        <div id="row_of_expense">SAROJINI NAIDU MARG,LUCKNOW,</div>
                        <div id="row_of_expense">Lucknow, Uttar Pradesh, 226016</div>
                        <div id="row_of_expense">Contact No- +91 8009855566</div>
                        <div id="row_of_expense">GSTIN: A760517063</div>
                        <div id="row_of_expense_1">{this.props.show_Expenses_info.Name_Of_Customer}</div>
                    </div>
                    <div className="column_of_expense_right">
                        <img src="surelocal.png" width="100" height="100"></img>
                        <div>Date: {this.props.show_Expenses_info.Date}</div>
                        <div>No: {this.props.show_Expenses_info.Receipt_No}</div>
                    </div>
                </div>
                  <table id="my">
                       <thead>
                            <tr>
                                <th id="expense_th">Expense_Name</th>
                                <th id="expense_th">Expense_Type</th>
                                <th id="expense_th">Expense_Amount</th>
                                <th id="expense_th">See_Bill</th>
                            </tr>
                       </thead>
                       <tbody>
                            {this.fetch_list_data()}
                       </tbody>
                  </table>
                  <div className="row" id="my">
                        <div className="column_left">
                            <div id="row_of_expense_1"> Notes</div>
                            <div id="row_of_expense_2"> Looking forward for your business</div>
                            <div id="row_of_expense_3"> Thank You </div>
                            <div id="row_of_expense_4"> Authorised Signature</div>
                        </div>
                        <div className="column_right">
                            <div>Total after Discount: {this.props.show_Expenses_info.Total_expense_cost}</div>
                        </div>
                  </div>
            </div>
        );
     }

//      calculate_total = () => {
//        let lst = this.props.show_Expenses_info.Data;
//        console.log("================================",lst);
////            lst.map(val => {
////                console.log(val.Data[0].name);
////            });
//        let sum = 0;
//        for(let i=0;i<lst.length;i++){
//             sum += lst[i].pending;
//        }
//        return sum;
//     }
     //      fetch_list_data = () => {
//            let lst = this.props.show_Expenses_info;
////            console.log("================================",lst.Data);
//            return lst.Data.map((value,index) => {
//                 return (
//                 <tr key={value.index}>
//                    <td><input value = {value.name} name= "name" id="expense_td" readonly/></td>
//                    <td><input value = {value.type} name = "type" readonly/></td>
//                    <td><input value = {value.pending} name = "pending" readonly/></td>
//                    <td><input value = {value.final} name = "final" readonly/></td>
//                    <td><input value = {value.quantity} name = "quantity" readonly/></td>
//                    <td><input value = {value.discount} name = "discount" readonly/></td>
//                 </tr>
//                );
//            })
//    }

//         render(){
//            console.log("IN SHOW USERS INFO PAGE", this.props.show_Expenses_info);
//            return (
//            <div id="expense_div">
//                <div className="row" id="my">
//                    <div className="column_of_expense_left">
//                        <div id="row_of_expense">INVOICE</div>
//                        <div id="row_of_expense">SURELOCAL SUPPLY CHAIN pvt.ltd</div>
//                        <div id="row_of_expense">SAROJINI NAIDU MARG,LUCKNOW,</div>
//                        <div id="row_of_expense">Lucknow, Uttar Pradesh, 226016</div>
//                        <div id="row_of_expense">Contact No- +91 8009855566</div>
//                        <div id="row_of_expense">GSTIN: A760517063</div>
//                    </div>
//                    <div className="column_of_expense_right">
//                        <img src="surelocal.png" width="100" height="100"></img>
//                        <div>Date: {this.state.current_date}</div>
//                        <div>No: 123456789</div>
//                    </div>
//                </div>
//                  <table id="my">
//                       <thead>
//                            <tr>
//                                <th id="expense_th">Expense_Name</th>
//                                <th id="expense_th">Expense_Type</th>
//                                <th id="expense_th">Final Amount</th>
//                                <th id="expense_th">Inital Amount</th>
//                                <th id="expense_th">Expense_Qty</th>
//                                <th id="expense_th">Expense_Discount</th>
//                            </tr>
//                       </thead>
//                       <tbody>
//                            {this.fetch_list_data()}
//                       </tbody>
//                  </table>
//                  <div className="row" id="my">
//                        <div className="column_left">
//                            <div id="row_of_expense_1"> Notes</div>
//                            <div id="row_of_expense_2"> Looking forward for your business</div>
//                            <div id="row_of_expense_3"> Thank You </div>
//                            <div id="row_of_expense_4"> Authorised Signature</div>
//                        </div>
//                        <div className="column_right">
//                            <div>Discount: {this.calculate_total_discount()}</div>
//                            <div>Total after Discount: {this.calculate_total()}</div>
//                        </div>
//                  </div>
//            </div>
//            );
//         }
}

export default Show_Expenses_info;
