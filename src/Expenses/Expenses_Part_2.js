import React from 'react';
import db from '../Database/firestore.js'
import * as page from '../enum/enum.js';
//import { DropdownButton,Dropdown } from 'react-bootstrap';
import Downshift from 'downshift'
import Show_Expenses_info from './Show_expenses_info.js';
//import Build_logic from '../Building_Expense_logic.js';
import Build_logic_Pt_2 from './Expense_logic_part_2.js';

class Expenses_part_2 extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            flag : page.Pages.Show_table,
            name : "",
            type : "",
            pending_amt : 0,
            final_amt : 0,
            discount : 0,
            quantity : 1,
            list : [],
            name_Input : {},
            original_final_amt : 0,
            search_results : [],
            dismiss_Dropbox : false,
            fetched_data_expenses : {},
            start_calc : false,
            receipt_no : 0,
        };
    }

    componentDidMount(){
        this.Get_data_from_firestore();
        console.log('GrandChild did mount.');
    }

//    fill_Entry = (data) => {
//        var search = this.state.search_results;
//        console.log("Tap ====",data.Name);
//        this.setState({
//            ...this.state,
//            name : data.Name,
//            type : data.Type,
//            pending_amt : data.Pending_Amount,
//            original_final_amt : data.Final_Amount,
//            final_amt : data.Final_Amount,
//            dismiss_Dropbox : true,
//        });
//    }

//    Dropbox = () => {
//        var list = this.state.search_results;
//        var response = []
//        if(list.length > 0 && this.state.dismiss_Dropbox === false){
//            list.forEach((li,index) => {
//                console.log("Search", li.Name);
//                var entry =  (
//                    <div id={li.id} onClick={this.fill_Entry.bind(this,li)}>
//                        <label>{li.Name}</label>
//                    </div>
//                );
//                response.push(entry);
//            });
//        }
//        return response;
//    }

//    NameChange = (event) => {
//        var input = event.target.value.toLowerCase(); // for matching a Single word.
//        var names = this.state.list;
//        var search = names.filter(entry => {
//            return entry.Name.toLowerCase().includes(input);
//        });
//        console.log(search);
//        this.setState({
//            ...this.state,
//            name : event.target.value,
//            search_results : search,
//            dismiss_Dropbox : false,
//        });
//    }
//
//
//    TypeChange = (event) => {
//        this.setState({
//            ...this.state,
//            type : event.target.value,
//        });
//    }
//
//    Pending_Amount_Changed = (event) => {
//        this.setState({
//            ...this.state,
//            pending_amt : event.target.value,
//        });
//    }
//
//    Final_Amount_Changed = (event) => {
//        this.setState({
//            ...this.state,
//            final_amt : event.target.value,
//            original_final_amt : event.target.value,
//        });
//    }
//
//     calculate_qty = (e,qty) => {
//            e.preventDefault();
//            console.log("In CAL QTY");
//            var amt = this.state.original_final_amt;
//            var dis = this.state.discount;
//            var new_amt = (amt * qty) - dis;
//            console.log(new_amt);
//            return new_amt;
//    }
//
//        calculate_dis = (e,dis) => {
//            console.log("In CAL DIS");
//            var amt = this.state.original_final_amt;
//            var qty = this.state.quantity;
//            var new_amt = (amt * qty) - dis;
//            console.log(new_amt);
//            return new_amt;
//        }
//
//     Quantity_Changed = (event) => {
//       var my = this.calculate_qty(event,event.target.value);
//       this.setState({
//            ...this.state,
//            quantity : event.target.value,
//            final_amt : my,
//       });
//    }
//
//     Discount_Changed = (event) => {
//        var my = this.calculate_dis(event,event.target.value);
//        this.setState({
//            ...this.state,
//            discount : event.target.value,
//            final_amt : my,
//        });
//
//    }
//
//    Add_Data_to_Firestore = (event) => {
//        event.preventDefault();
//        db.firestore().collection('Expenses').add({
//            "Name" : this.state.name,
//            "Type" : this.state.type,
//            "Pending_Amount" : this.state.pending_amt,
//            "Final_Amount" : this.state.final_amt,
//            "Qty" : this.state.quantity,
//            "Discount" : this.state.discount,
//        }).then(function(){
//            console.log("Data Successfully Entered");
//        }).catch(function(err){
//            console.log(err);
//        });
//        this.setState({
//              ...this.state,
//              flag : page.Pages.Show_table,
//        });
//    }

     Get_data_from_firestore = () => {
         db.firestore().collection("Expense_Data").onSnapshot(it => {
              var response = [];
              it.forEach((doc) => {
                response.push({...doc.data(),id: doc.id})
              })
              console.log(response);
              this.setState({
                    ...this.state,
                    list : response,
              })
            console.log(this.state.list);
        });
    }

     fetch_list_data = () => {
        var l = this.state.list;
        var response = [];
        l.map((value,index) => {
            console.log("FETCH LIST DATAAAAAAAAAA",value.Expenses[0].name);
              var b = (
                   <tr key={value.id} onClick={this.fetch_Particular_Expense_Details.bind(this,index)} id="customer_tr">
                       <td id="customer_td">{value.Payment_Type}</td>
                       <td id="customer_td">{value.Expenses[0].name}</td>
                       <td id="customer_td">{value.Expenses[0].type}</td>
                       <td id="customer_td">{value.Total_expense_cost}</td>
                       <td id="customer_td">{value.Date}</td>
                   </tr>
              );
              response.push(b);
        });
        return response;
    }

    fetch_Particular_Expense_Details = (index) => {
        var data_list  = this.state.list[index];
            console.log("The Value IS:", data_list);
            this.setState({
                 ...this.state,
                 flag : page.Pages.Show_table_values_Expenses,
                 fetched_data_expenses : data_list,
            });
    }

    Table_Of_Expense = () => {
        return (
            <div id="customer_div">
                <button onClick={this.buttonClick} id="customer_add_btn">Add Expenses</button>
                <div id="dummy">dummy</div>
                    <table id="customer_table">
                       <thead>
                            <tr id="customer_tr">
                                <th id="customer_th">Expense_Payment_type</th>
                                <th id="customer_th">Expense_Name</th>
                                <th id="customer_th">Expense_Type</th>
                                <th id="customer_th">Expense_Amount</th>
                                <th id="customer_th">Exp_Date</th>
                            </tr>
                       </thead>
                       <tbody>
                            {this.fetch_list_data()}
                       </tbody>
                  </table>
              </div>
        );
    }

    buttonClick = () => {
        console.log("I Am CLICKED!!!!!!!");
        let number = Math.random();
        this.setState({
            ...this.state,
            flag : page.Pages.Input_user,
            receipt_no : Math.trunc(number * 100)+1,
        });
    }

    Check = () => {
        var check = this.state.flag;
        if(check === page.Pages.Input_user){
            return <Build_logic_Pt_2 build_logic = {this.state.receipt_no}/>;
        }else if(check === page.Pages.Show_table){
            return this.Table_Of_Expense();
        }else if(check === page.Pages.Show_table_values_Expenses){
            return <Show_Expenses_info show_Expenses_info ={this.state.fetched_data_expenses}/>;
        }else{
            return this.Table_Of_Expense();
        }
    }

    render(){
        return(
            <div>
                {this.Check()}
            </div>
        );
    }

}

export default Expenses_part_2;