import React from 'react';
import db from '../Database/firestore.js'
import * as page from '../enum/enum.js';
import Expenses_Part_2 from './Expenses_Part_2.js';
import imageCompression from 'browser-image-compression';

class Build_logic_Pt_2 extends React.Component{
    constructor(props){
        super(props);
        var today = new Date();
        var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        this.state = {
            expense : [{name: "", type: "",amount: 0,img: ""}],
            flag : page.Pages.Show_table,
            list : [],
            original_final_amt : 0,
            search_results : [],
            fetched_data_expenses : {},
            init_ind : 0,
            dismiss_Dropbox : false,
            dismiss_Dropbox_expense : false,
            counter : 0,
            current_date : date,
            list_of_user : [],
            search_result_User : [],
            Payment_Type : "",
            total_Price : 0,
            expense_type : ["Transport","Food","Petrol",],
            expense_type_1 : [],
            receipt_no : this.props.build_logic,
        }
    }

    componentDidMount(){
        this.Get_data_from_firestore();
        this.Get_data_from_firestore_User();
        console.log('GrandChild did mount.');
    }

     Get_data_from_firestore = () => {
         db.firestore().collection("Expenses").onSnapshot(it => {
              var response = [];
              it.forEach((doc) => {
                response.push({...doc.data(),id: doc.id})
              })
              console.log(response);
              this.setState({
                    ...this.state,
                    list : response,
              })
            console.log("List Data===== ",this.state.list);
        });
    }

        Get_data_from_firestore_User = () => {
         db.firestore().collection("Users").onSnapshot(it => {
              var response = [];
              it.forEach((doc) => {
                response.push({...doc.data(),id: doc.id})
              })
              this.setState({
                    ...this.state,
                    list_of_user : response,
              })
            console.log(this.state.list_of_user);
        });
    }

     Dropbox = () => {
        var list = this.state.search_result_User;
        console.log("IN DROPBOX========",list);
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                console.log("Search", li.Name);
                var entry =  (
                    <div id={li.id} onClick={this.fill_Entry_User.bind(this,li)}>
                        <label>{li.Name}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }

    fill_Entry_User = (data) => {
        var name = data.Name;
        console.log("NAME OF USER +++++", name);
        this.setState({
            ...this.state,
            Payment_Type : name,
            dismiss_Dropbox : true,
        })
    }

    select_User = () => {
        return (
            <input id="User_selection_Box" name="Payment_Type" onChange={this.Payment_Type_Change.bind(this)} value={this.state.Payment_Type}/>
        )
    }

    Payment_Type_Change = (event) => {
        const{ name, value } = event.target;
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list_of_user;
        var search = names.filter(entry => {
            return entry.Name.toLowerCase().includes(input);
        });
        this.setState({
            ...this.state,
            dismiss_Dropbox : false,
            search_result_User : search,
            Payment_Type : value,
        });
    }

//    Expense_type_dropdown = () => {
//        return (
//                    <select name="payment" id="Pay" value={this.state.select_Payment_type} onChange={this.getPay.bind(this)}>
//                        <option value="Google Pay">Google Pay</option>
//                        <option value="Cheque">Cheque</option>
//                        <option value="Cash">Cash</option>
//                        <option value="Card">Card</option>
//                    </select>
//        )
//    }

//    getPay = (e) => {
//        const{ name, value} = e.target;
//        this.setState({
//            ...this.state,
//            select_Payment_type : value,
//        })
//    }



    fetch_list_data = () => {
            return this.state.expense.map((value,index) => {
                 return (
                 <tr key={value.index}>
                    <td><input value = {value.name} name= "name" onChange={this.NameChange.bind(this,index)} size="50"/></td>
                    <td><input value = {value.type} name= "type" onChange={this.TypeChange.bind(this,index)} size="38"/></td>
                    <td><input value = {value.amount} name = "amount" onChange={this.Amount_Changed.bind(this,index)} /></td>
                    <td><input type="file" name="img" src={value.img} onChange={this.file_change.bind(this,index)} /></td>
                 </tr>
                );
            })
    }


    file_change = async (i,event) =>{
         const options = {
            maxSizeMB: 1,
            maxWidthOrHeight: 1920,
            useWebWorker: true
         };
        console.log("event",event.target.files);
        const { name, value } = event.target;
        // as it is a file type input, files[0] contains actual file while files[1] contains length of file
        var img = event.target.files[0];
        var img1 = event.target.files[0].name;
        if(img.size > 1000000){
            const compressedFile = await imageCompression(img, options);
//        var file = this.state.img; // use the Blob or File API
            db.storage().ref().child("PHOTOS").child(value).put(compressedFile).then(function(snapshot) {
                console.log('Uploaded a blob or file!',snapshot);
            }).catch((err) => {
                console.log(err);
            });
        }else{
            console.log("Value",event.target.files[0]);
            db.storage().ref().child("PHOTOS").child(value).put(img).then(function(snapshot) {
                console.log('Uploaded a blob or file!',snapshot);
            }).catch((err) => {
                console.log(err);
            });
        }
        var abc = this.state.expense;
        let expense = [...abc];
        let exp = {...expense[i]};
        exp.img = "PHOTOS/"+value;
        expense[i] = exp;
        this.setState({
            ...this.state,
            expense : expense,
        });
    }

    NameChange = (i,event) => {
        const { name, value } = event.target;
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
        });
    }


    Dropbox_Of_expense = () => {
        var list = this.state.expense_type_1;
        console.log("IN DROPBOX========",list);
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox_expense === false){
            list.forEach((li,index) => {
                console.log("Search", li);
                var entry =  (
                    <div id={li} onClick={this.fill_Entry_Expense.bind(this,li)}>
                        <label>{li}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }

    fill_Entry_Expense = (data) => {
//        var name = data.Name;
        let id = this.state.init_ind;
        let expenses = [...this.state.expense];
        let expense = {...expenses[id]};
        expense.type = data;
        expenses[id] = expense;
        console.log("Type OF USER +++++", expenses);
        this.setState({
            ...this.state,
            expense : expenses,
            dismiss_Dropbox_expense : true,
        })
    }

     TypeChange = (i, event) => {
        const { name, value } = event.target;
        var input = value.toLowerCase();
        var expense_type = this.state.expense_type;
        var search = expense_type.filter(entry => {
            return entry.toLowerCase().includes(input);
        });
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
            expense_type_1 : search,
            init_ind : i,
            dismiss_Dropbox_expense : false,
        });
     }

     Amount_Changed = (i,e) => {
        const { name, value } = e.target;
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
            counter : 1,
        });
    }

     calculate_total = () => {
        let exp = this.state.expense;
        let sum = 0;
        let count = this.state.counter;
        if(count > 0){
            for(let i=0;i<exp.length;i++){
                sum += parseInt(exp[i].amount);
            }
        }
        return sum;
     }

    addClick(){
        console.log(JSON.stringify(this.state.expense));
        console.log("payment Type======",this.state.select_Payment_type);
//        console.log("index======",this.state.init_ind);
        this.setState((prevState) => ({
            expense : [...prevState.expense,{name: "", type: "",amount: 0,img:""}],
        }));
  }

  Add_Data_to_Firestore = (event) => {
        let sum = this.calculate_total();
        var data = this.state.expense;
        var pay_type_of_user = this.state.Payment_Type.toString();
        var pay_type = this.state.select_Payment_type;
        var Date = this.state.current_date;
        let receipt_no = this.props.build_logic;
        let Data = data.map(val => {
            console.log("DAAAAAAAAAAAAAAAAAAAA",val)
        });
        db.firestore().collection('Expense_Data').add({
            "Payment_Type" : pay_type_of_user,
            "Total_expense_cost" : sum,
            "Date" : Date,
            "Expenses" : data,
            "Receipt_No" : receipt_no,
        }).then(function(){
            console.log("Data Successfully Entered");
        }).catch(function(err){
            console.log(err);
        });
        this.setState({
                ...this.state,
                flag : page.Pages.Expenses,
            });
    }

        Table_Of_Expense = () => {
        return (
            <div id="expense_div">
                <div className="row" id="my">
                    <div className="column_of_expense_left">
                        <div id="row_of_expense">INVOICE</div>
                        <div id="row_of_expense">SURELOCAL SUPPLY CHAIN pvt.ltd</div>
                        <div id="row_of_expense">SAROJINI NAIDU MARG,LUCKNOW,</div>
                        <div id="row_of_expense">Lucknow, Uttar Pradesh, 226016</div>
                        <div id="row_of_expense">Contact No- +91 8009855566</div>
                        <div id="row_of_expense">GSTIN: A760517063</div>
                        <div id="row_of_expense_1">{this.select_User()}</div>
                        <div>
                         {this.Dropbox()}
                        </div>
                    </div>
                    <div className="column_of_expense_right">
                        <img src="surelocal.png" width="100" height="100"></img>
                        <div>Date: {this.state.current_date}</div>
                        <div>No: {this.props.build_logic}</div>
                    </div>
                </div>
                  <table id="my">
                       <thead>
                            <tr>
                                <th id="expense_th">Expense_Name</th>
                                <th id="expense_th">Expense_Type</th>
                                <th id="expense_th">Expense_Amount</th>
                                <th id="expense_th">Upload_Bill</th>
                            </tr>
                       </thead>
                       <tbody>
                            {this.fetch_list_data()}
                       </tbody>
                  </table>
                  <div id="DropBox_expense">
                         {this.Dropbox_Of_expense()}
                        </div>
                  <input type='button' value='add more' onClick={this.addClick.bind(this)}/>
                  <div className="row" id="my">
                        <div className="column_left">
                            <div id="row_of_expense_1"> Notes</div>
                            <div id="row_of_expense_2"> Looking forward for your business</div>
                            <div id="row_of_expense_3"> Thank You </div>
                            <div id="row_of_expense_4"> Authorised Signature</div>
                        </div>
                        <div className="column_right">
                            <div>Total after Discount: {this.calculate_total()}</div>
                        </div>
                  </div>
                  <input type='button' value='Submit' onClick={this.Add_Data_to_Firestore.bind(this)}/>
            </div>
        );
    }

    Check = () => {
        var check = this.state.flag;
        if(check === page.Pages.Expenses){
            return <Expenses_Part_2 />;
        }else{
            return this.Table_Of_Expense();
        }
    }

    render(){
        return(
            <div>
                {this.Check()}
            </div>
        );
    }
}


export default Build_logic_Pt_2;