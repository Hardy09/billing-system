export const Pages = {
     Input_user : 'inputUser',
     Show_table : 'showTable',
     Show_table_values : 'showTableValues',
     Expenses : 'expenses',
     Customers : 'customers',
     Show_table_values_Expenses : 'showtablevaluesExpenses',
     Testing : 'Testing',
     Check_for_User : 'Check_for_User',
}