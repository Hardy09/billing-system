import React from 'react';
import db from './Database/firestore.js'
import * as page from './enum/enum.js';

class Build_logic extends React.Component{
    constructor(props){
        super(props);
        var today = new Date();
        var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        this.state = {
            expense : [{name: "", type: "",pending: 0,final: 0,quantity: 1,discount: 0}],
            flag : page.Pages.Show_table,
            list : [],
            original_final_amt : 0,
            search_results : [],
            dismiss_Dropbox : false,
            fetched_data_expenses : {},
            start_calc : false,
            init_ind : 0,
            dismiss_Dropbox : false,
            counter : 0,
            current_date : date,
            list_of_user : [],
            search_result_User : [],
            User : "",
        }
    }

    componentDidMount(){
        this.Get_data_from_firestore();
//        this.Get_data_from_firestore_User();
        console.log('GrandChild did mount.');
    }

     Get_data_from_firestore = () => {
         db.firestore().collection("Expenses").onSnapshot(it => {
              var response = [];
              it.forEach((doc) => {
                response.push({...doc.data(),id: doc.id})
              })
              console.log(response);
              this.setState({
                    ...this.state,
                    list : response,
              })
            console.log("List Data===== ",this.state.list);
        });
    }


    fill_Entry = (data) => {
        var search = this.state.search_results;
        console.log("Tap ====",data.Name);

        let index_of_list = this.state.init_ind;
        console.log("index===== ", this.state.init_ind);

        let expenses = [...this.state.expense];
        let expense = {...expenses[index_of_list]};
//        let expense = this.state.obj;
        expense.name = data.Name;
        expense.type = data.Type;
        expense.pending = data.Pending_Amount;
        expense.final = data.Final_Amount;
        expenses[index_of_list] = expense;
        console.log("Data from firestore is=========",expenses[index_of_list]);
        var amt = expense.final;
        this.setState({
            ...this.state,
            expense : expenses,
            original_final_amt : amt,
            dismiss_Dropbox : true,
        });
    }

    Dropbox = () => {
        var list = this.state.search_results;
        console.log("IN DROPBOX========",list);
        var response = []
        if(list.length > 0 && this.state.dismiss_Dropbox === false){
            list.forEach((li,index) => {
                console.log("Search", li.Name);
                var entry =  (
                    <div id={li.id} onClick={this.fill_Entry.bind(this,li)}>
                        <label>{li.Name}</label>
                    </div>
                );
                response.push(entry);
            });
        }
        return response;
    }
//        Get_data_from_firestore_User = () => {
//         db.collection("Users").onSnapshot(it => {
//              var response = [];
//              it.forEach((doc) => {
//                response.push({...doc.data(),id: doc.id})
//              })
//              this.setState({
//                    ...this.state,
//                    list_of_user : response,
//              })
//            console.log(this.state.list_of_user);
//        });
//    }

//     Dropbox = () => {
//        var list = this.state.search_result_User;
//        console.log("IN DROPBOX========",list);
//        var response = []
//        if(list.length > 0 && this.state.dismiss_Dropbox === false){
//            list.forEach((li,index) => {
//                console.log("Search", li.Name);
//                var entry =  (
//                    <div id={li.id} onClick={this.fill_Entry_User.bind(this,li)}>
//                        <label>{li.Name}</label>
//                    </div>
//                );
//                response.push(entry);
//            });
//        }
//        return response;
//    }

//    fill_Entry_User = (data) => {
//        this.setState({
//            ...this.state,
//            User : data.Name,
//            dismiss_Dropbox : true,
//        })
//    }
//
//    select_User = () => {
//        return (
//            <input id="User_selection_Box" name="User" onChange={this.User_Change.bind(this)} value={this.state.User}/>
//        )
//    }
//
//    User_Change = (event) => {
//        const{ name, value } = event.target;
//        var input = value.toLowerCase(); // for matching a Single word
//        var names = this.state.list_of_user;
//        var search = names.filter(entry => {
//            return entry.Name.toLowerCase().includes(input);
//        });
//        this.setState({
//            ...this.state,
//            dismiss_Dropbox : false,
//            search_result_User : search,
//            User : value,
//        });
//    }


    fetch_list_data = () => {
            return this.state.expense.map((value,index) => {
                 return (
                 <tr key={value.index}>
                    <td><input value = {value.name} name= "name" onChange={this.NameChange.bind(this,index)} id="expense_td"/></td>
                    <td><input value = {value.type} name = "type" onChange={this.TypeChange.bind(this,index)}/></td>
                    <td><input value = {value.pending} name = "pending" onChange={this.Pending_Amount_Changed.bind(this,index)} /></td>
                    <td><input value = {value.final} name = "final" onChange={this.Final_Amount_Changed.bind(this,index)}/></td>
                    <td><input value = {value.quantity} name = "quantity" onChange={this.Quantity_Changed.bind(this,index)}/></td>
                    <td><input value = {value.discount} name = "discount" onChange={this.Discount_Changed.bind(this,index)}/></td>
                 </tr>
                );
            })
    }


    NameChange = (i,event) => {
        const { name, value } = event.target;
        var input = value.toLowerCase(); // for matching a Single word
        var names = this.state.list;
        var search = names.filter(entry => {
            return entry.Name.toLowerCase().includes(input);
        });
        console.log("search ====",search);
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
            search_results : search,
            init_ind : i,
            dismiss_Dropbox : false,
        });
        console.log("IN HANDLE CHANGE SEARCH RESULT", this.state.search_results);
    }

     TypeChange = (i, event) => {
        const { name, value } = event.target;
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
        });
     }

     Pending_Amount_Changed = (i,e) => {
        const { name, value } = e.target;
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
        });
    }

    Final_Amount_Changed = (i,event) => {
        const { name, value } = event.target;
        var abc = this.state.expense;
        let expense = [...abc];
        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
        });
    }

     calculate_qty = (i,qty,amt) => {
            console.log("In CAL QTY");
            let index_of_list = this.state.init_ind;
            let expenses = [...this.state.expense];
            let expense = {...expenses[index_of_list]};
//            console.log("Amount for quantity",i,"IN QTY SECTION IS ",amt);
            var dis = expense.discount;
            var new_amt = (amt * qty) - dis;
            return new_amt;
    }

        calculate_dis = (i,dis,amt) => {
            console.log("In CAL DIS");
            let index_of_list = this.state.init_ind;
            let expenses = [...this.state.expense];
            let expense = {...expenses[index_of_list]};
            console.log("Amount for DISCOUNT=======",i,"IN QTY SECTION IS ",amt);
            var qty = expense.quantity;
            var new_amt = (qty * amt) - dis;
            console.log("PRICE AFTER DISCOUNT====== ",new_amt);
            return new_amt;
        }

     calculate_total = () => {
        let exp = this.state.expense;
        let sum = 0;
        let count = this.state.counter;
        if(count > 0){
            for(let i=0;i<exp.length;i++){
                sum += exp[i].pending;
            }
        }
        return sum;
     }

     calculate_total_discount = () => {
        let exp = this.state.expense;
        let s = 0;
        let count = this.state.counter;
        if(count > 0){
            for(let i=0;i<exp.length;i++){
                s += parseInt(exp[i].discount);

            }
        }
        console.log("Total Discount====",s);
        return s;
     }

     Quantity_Changed = (i,event) => {
//       console.log("IN QUANTITY=============================",i);
       const { name, value } = event.target;
        var abc = this.state.expense;
        let expense = [...abc];
        let exp = {...expense[i]};
        var new_price = this.calculate_qty(i,value,exp.final);
//        console.log("New_price_Quantity=== ",new_price);
        exp.pending = new_price;
        expense[i] = exp;

        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
            counter : 1,
        });
    }

     Discount_Changed = (i,event) => {
        console.log("IN DISCOUNT=============================",i);
        const { name, value } = event.target;
        var abc = this.state.expense;
        let expense = [...abc];
        let exp = {...expense[i]};
        var new_price = this.calculate_dis(i,value,exp.final);
        console.log("New_price_Discount=== ",new_price);
        exp.pending = new_price;
        expense[i] = exp;


        // expense[i] represent that particular value which is to be added
        expense[i] = {...expense[i],[name]:value}; // assigning values in array & spreading name object value before type field.
        this.setState({
            ...this.state,
            expense : expense,
            count : 2,
        });
    }

  addClick(){
        console.log(JSON.stringify(this.state.expense));
        console.log("object======",this.state.obj);
        console.log("index======",this.state.init_ind);
        this.setState((prevState) => ({
            expense : [...prevState.expense,{name: "", type: "",pending: 0,final: 0,quantity: 1,discount: 0}]
        }));
  }

  Add_Data_to_Firestore = (event) => {
        var data = this.state.expense;
        console.log("MMMMMMMMMMMMMMMMMMM",data);
        let Data = data.map(val => {
            console.log("DAAAAAAAAAAAAAAAAAAAA",val)
        });
        event.preventDefault();
        db.collection('Data').add({
            "Data" : data
        }).then(function(){
            console.log("Data Successfully Entered");
        }).catch(function(err){
            console.log(err);
        });
    }

//        Table_Of_Expense = () => {
//        return (
//            <div id="expense_div">
//                <div className="row" id="my">
//                    <div className="column_of_expense_left">
//                        <div id="row_of_expense">INVOICE</div>
//                        <div id="row_of_expense">SURELOCAL SUPPLY CHAIN pvt.ltd</div>
//                        <div id="row_of_expense">SAROJINI NAIDU MARG,LUCKNOW,</div>
//                        <div id="row_of_expense">Lucknow, Uttar Pradesh, 226016</div>
//                        <div id="row_of_expense">Contact No- +91 8009855566</div>
//                        <div id="row_of_expense">GSTIN: A760517063</div>
//                        <div id="row_of_expense_1">{this.select_User()}</div>
//                        <div>
//                         {this.Dropbox()}
//                        </div>
//                    </div>
//                    <div className="column_of_expense_right">
//                        <img src="surelocal.png" width="100" height="100"></img>
//                        <div>Date: {this.state.current_date}</div>
//                        <div>No: 123456789</div>
//                    </div>
//                </div>
//                  <table id="my">
//                       <thead>
//                            <tr>
//                                <th id="expense_th">Expense_Name</th>
//                                <th id="expense_th">Expense_Type</th>
//                                <th id="expense_th">Expense_Discount</th>
//                            </tr>
//                       </thead>
//                       <tbody>
//                            {this.fetch_list_data()}
//                       </tbody>
//                  </table>
//                  <input type='button' value='add more' onClick={this.addClick.bind(this)}/>
//                  <div className="row" id="my">
//                        <div className="column_left">
//                            <div id="row_of_expense_1"> Notes</div>
//                            <div id="row_of_expense_2"> Looking forward for your business</div>
//                            <div id="row_of_expense_3"> Thank You </div>
//                            <div id="row_of_expense_4"> Authorised Signature</div>
//                        </div>
//                        <div className="column_right">
//                            <div>Discount: {this.calculate_total_discount()}</div>
//                            <div>Total after Discount: {this.calculate_total()}</div>
//                        </div>
//                  </div>
//                  <input type='button' value='Submit' onClick={this.Add_Data_to_Firestore.bind(this)}/>
//            </div>
//        );
//    }
    Table_Of_Expense = () => {
        return (
            <div id="expense_div">
                <div className="row" id="my">
                    <div className="column_of_expense_left">
                        <div id="row_of_expense">INVOICE</div>
                        <div id="row_of_expense">SURELOCAL SUPPLY CHAIN pvt.ltd</div>
                        <div id="row_of_expense">SAROJINI NAIDU MARG,LUCKNOW,</div>
                        <div id="row_of_expense">Lucknow, Uttar Pradesh, 226016</div>
                        <div id="row_of_expense">Contact No- +91 8009855566</div>
                        <div id="row_of_expense">GSTIN: A760517063</div>
                    </div>
                    <div className="column_of_expense_right">
                        <img src="surelocal.png" width="100" height="100"></img>
                        <div>Date: {this.state.current_date}</div>
                        <div>No: 123456789</div>
                    </div>
                </div>
                  <table id="my">
                       <thead>
                            <tr>
                                <th id="expense_th">Expense_Name</th>
                                <th id="expense_th">Expense_Type</th>
                                <th id="expense_th">Final Amount</th>
                                <th id="expense_th">Inital Amount</th>
                                <th id="expense_th">Expense_Qty</th>
                                <th id="expense_th">Expense_Discount</th>
                            </tr>
                       </thead>
                       <tbody>
                            {this.fetch_list_data()}
                       </tbody>
                       <div>
                         {this.Dropbox()}
                        </div>
                  </table>
                  <input type='button' value='add more' onClick={this.addClick.bind(this)}/>
                  <div className="row" id="my">
                        <div className="column_left">
                            <div id="row_of_expense_1"> Notes</div>
                            <div id="row_of_expense_2"> Looking forward for your business</div>
                            <div id="row_of_expense_3"> Thank You </div>
                            <div id="row_of_expense_4"> Authorised Signature</div>
                        </div>
                        <div className="column_right">
                            <div>Discount: {this.calculate_total_discount()}</div>
                            <div>Total after Discount: {this.calculate_total()}</div>
                        </div>
                  </div>
                  <input type='button' value='Submit' onClick={this.Add_Data_to_Firestore.bind(this)}/>
            </div>
        );
    }

    render(){
        return(
            <div>
                {this.Table_Of_Expense()}
            </div>
        );
    }
}


export default Build_logic;