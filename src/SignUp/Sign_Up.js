import React from 'react';
import logo from '../logo.svg';
import '../css/App.css';
import db from '../Database/firestore.js'
import * as page from '../enum/enum.js';
import App from '../Customers/App.js';

class Sign_Up extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Customers_Expenses_flag : true,
            res : [],
            flag : page.Pages.Check_for_User,
            Password : "",
            Email : "",
            list : [],
            fetched_data : {},
        }
    }

    PasswordChange = (event) => {
        this.setState({
            ...this.state,
            Password : event.target.value,
        });
    }

    EmailChange = (event) => {
        this.setState({
            ...this.state,
            Email : event.target.value,
        });
    }

    Add_User_to_Firestore = (event) =>{
        var mythis = this;
        event.preventDefault();
        var password = this.state.Password;
        var email = this.state.Email;
        db.auth().signInWithEmailAndPassword(email,password).then(it => {
            console.log("In Get.....");
            console.log(it);
             mythis.setState({
                ...mythis.state,
                flag : page.Pages.Input_user,
            });
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorMessage);
        });

    }

    Check = () => {
        var fl = this.state.flag;
//        console.log("Flagg",fl);
        if(fl === page.Pages.Check_for_User){

            return this.InputUser();
        }else if(fl === page.Pages.Input_user){

            return <App/>;
        }
    }

    InputUser = () => {
        return (
            <form className="form-horizontal" onSubmit={this.Add_User_to_Firestore}>
                 <div className="form-group">
                    <label className="control-label col-sm-2" for="Email">Email:</label>
                    <div class="col-sm-10">
                        <input type="Email" className="form-control" placeholder="Enter Email"
                        value= {this.state.Email} onChange= {this.EmailChange} />
                    </div>
                </div>
                <div className="form-group">
                    <label class="control-label col-sm-2" for="name">Password:</label>
                    <div className="col-sm-10">
                        <input type="Name" className="form-control" placeholder="Enter Password" value= {this.state.Password}
                        onChange= {this.PasswordChange} />
                    </div>
                </div>
                <button className="btn btn-primary">Submit</button>
            </form>
        );
    }



    render(){
        return(
            <div>
                 {this.Check()}
            </div>
        )
    }

}
export default Sign_Up;
